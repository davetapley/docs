---
title: How to work with Database Lab CLI
sidebar_label: Overview
slug: /guides/cli
---

## Guides
- [How to install and initialize Database Lab CLI](/docs/guides/cli/cli-install-init)
