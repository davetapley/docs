// Duplicates data from the blog.
const blog = [
  {
    link: 'blog/dle-2-2-release',
    date: '2021-02-22 06:45:00',
    title: 'Database Lab Engine 2.2 and Joe Bot 0.9',
    description: 'Database Lab Engine 2.2.0 and SQL Optimization Chatbot “Joe” 0.9.0 released: multiple pools for automated “logical” initialization, production timing estimation (experimental), and improved security.',
    image: '/assets/thumbnails/dle-2.2-blog.png',
  },
  {
    link: 'blog/dle-2-1-release',
    date: '2020-12-31 09:22:00',
    publishDate: '2020-12-31 09:22:00',
    title: 'Database Lab Engine 2.1',
    description: 'Database Lab Engine 2.1 released: automated physical and logical initialization, Amazon RDS PostgreSQL support, basic data transformation and masking',
    image: '/assets/thumbnails/dle-2.1-blog.png',
  },
  {
    link: 'blog/dle-2-0-release',
    date: '2020-11-11 11:20:20',
    publishDate: '2020-11-10 11:20:20',
    title: 'Database Lab Engine 2.0',
    description: 'Database Lab Engine 2.0 released: automated physical and logical initialization, Amazon RDS PostgreSQL support, basic data transformation and masking',
    image: '/assets/thumbnails/dle-2.0-blog.png',
  },
  /*{
    link: 'blog/plan-exporter',
    date: '2020-05-23 21:19:00',
    title: 'plan-exporter: visualize PostgreSQL EXPLAIN data right from psql',
    description: 'No more headache with copy-pasting huge plans from psql to explain.dalibo.com and explain.depesz.com',
    image: '/assets/plan-exporter2.png',
  },
  {
    link: '/blog/joe-0.7',
    date: '2020-05-18 11:16:00',
    title: 'Joe 0.7.0 released! New in this release: Web UI, Channel Mapping, and new commands',
    description: 'Secure and performant Web UI brings more flexibility, 1:1 communication, and visualization options',
    image: '/assets/joe-3-silhouettes.svg',
  },
  {
    link: '/blog/joe-0.6',
    date: '2020-03-23 10:42:00',
    title: 'Joe 0.6.0 supports hypothetical indexes',
    description: 'Have an index idea for a large table? Get a sneak peek of how SQL plan will look like using Joe\'s new command, "hypo", that provides support of hypothetical indexes',
    image: '/assets/joe-3-silhouettes.svg',
  },
  {
    link: '/blog/dblab-0.3',
    date: '2020-03-04 08:02:00',
    title: 'Database Lab Engine 0.3, supports both ZFS and LVM',
    description: 'Database Lab Engine 0.3: now LVM can be used instead of ZFS for thin cloning',
    image: '/assets/dblab.svg',
  },
  {
    link: '/blog/joe-0.5',
    date: '2020-02-26 21:42:00',
    title: 'Joe bot, an SQL query optimization assistant, updated to version 0.5.0',
    description: 'Postgres.ai team is proud to present version 0.5.0 of Joe bot, an SQL query optimization assistant',
    image: '/assets/joe-3-silhouettes.svg',
  },
  {
    link: '/blog/dblab-0.2',
    date: '2020-02-06 10:38:00',
    title: 'Database Lab Engine 0.2',
    description: 'Database Lab Engine updated to 0.2: everything in containers, better API and CLI',
    image: '/assets/dblab.svg',
  },
  {
    link: '/blog/dblab-first-release',
    date: '2020-01-28 14:15:00',
    title: 'The first public release of Database Lab Engine',
    description: 'Postgres.ai team is proud to announce the very first public release of Database Lab Engine',
    image: '/assets/dblab.svg',
  },*/
];

export default blog;
