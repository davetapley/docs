---
id: get-started
title: Database Lab Documentation
hide_title: false
sidebar_label: Documentation Home
slug: /
description: Database Lab is used to boost software development via enabling ultra-fast provisioning of databases of any size. Developers, DBAs, and QA engineers work with full-sized independent clones of PostgreSQL databases. Development and testing tasks are accomplished much faster, with more iterations done, with better quality achieved and with much less money spent.
keywords:
  - "postgres.ai clones"
  - "postgresql clones"
  - "Database Lab Engine"
  - "Dev/QA/Staging databases with superpowers"
  - "postgres-checkup"
  - "Joe bot for SQL optimization"
  - "SQL optinization on clones"
  - "thin clones for Postgres"
  - "database testing in CI/CD"
  - "postgres-checkup"
---

<table class='docs-home'>
  <tr>
    <td>
      <h3>Getting Started</h3>
      <ul>
        <li><a href='/docs/tutorials/database-lab-tutorial-amazon-rds'>Engine Setup for Amazon RDS</a></li>
        <li><a href='/docs/tutorials/database-lab-tutorial'>Engine Setup for any PostgreSQL database</a></li>
        <li><a href='/docs/tutorials/joe-setup'>Joe Bot Setup</a></li>
        <li><a href='/docs/questions-and-answers'>FAQ</a></li>
      </ul>
    </td>
    <td>
      <h3>Product & Use Cases</h3>
      <ul>
        <li><a href='/products/how-it-works'>How it Works</a></li>
        <li><a href='/products/database-migration-testing'>Database Migration Testing</a></li>
        <li><a href='/products/joe'>SQL Optimization</a></li>
        <li><a href='/products/realistic-test-environments'>Realistic Dev & Test Environments</a></li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>
      <h3>Security</h3>
      <ul>
        <li><a href='/docs/platform/security'>Platform Security</a></li>
        <li><a href='/docs/database-lab/masking'>Data Masking Setup & Configuration</a></li>
      </ul>
    </td>
    <td>
      <h3>Reference</h3>
      <ul>
        <li><a href='/docs/database-lab/config-reference'>Database Lab Engine (DLE) Configuration</a></li>
        <li><a href='/docs/joe-bot/config-reference'>Joe Bot Configuration</a></li>
      </ul>
    </td>
  </tr>
</table>
