---
title: Database Lab Engine administration
sidebar_label: Overview
slug: /guides/administration
description: How to administer Database Lab Engine
---

## Guides
- [How to configure PostgreSQL used by Database Lab Engine](/docs/guides/administration/postgresql-configuration)
- [How to manage Database Lab Engine](/docs/guides/administration/engine-manage)
- [How to manage Joe Bot](/docs/guides/administration/joe-manage)
- [Secure Database Lab Engine](/docs/guides/administration/engine-secure)
- [Set up machine for Database Lab Engine](/docs/guides/administration/machine-setup)
- [How to refresh data when working in the "logical" mode](/docs/guides/administration/logical-full-refresh)
- [Masking sensitive data in PostgreSQL logs when using CI Observer](/docs/guides/administration/ci-observer-postgres-log-masking)
