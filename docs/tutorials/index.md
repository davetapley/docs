---
title: Database Lab tutorials
slug: /tutorials
---

- [Database Lab tutorial for any PostgreSQL database](/docs/tutorials/database-lab-tutorial)
- [Database Lab tutorial for Amazon RDS](/docs/tutorials/database-lab-tutorial-amazon-rds)
- [Start using Joe Bot](/docs/tutorials/joe-setup)
