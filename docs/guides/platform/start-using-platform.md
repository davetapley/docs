---
title: Start using Database Lab (Postgres.ai) Platform
sidebar_label: Start using Platform
---

[Enter Postgres.ai](https://postgres.ai/signin/) using one of the following supported ways to authenticate: Google, LinkedIn, GitLab, GitHub.

When you first time do it, make sure to read and comply with [Postgres.ai Terms of the Service](https://postgres.ai/tos/).

Once you are in, you can create an organization (or use an automatically generated one). Alternatively, you can ask your colleague, who is already using Postgres.ai, to invite you to an existing organization. In the latter case, you will receive an email with the link clicking on which you will join that organization.

Organizations on the Postgres.ai platform can be considered as teams or companies. All activities happen in the context of an organization.

To start using Posgres.ai, you can choose one of two options:
1. Read and follow the [Database Lab Tutorial](/docs/tutorials/database-lab-tutorial) that covers Database Lab server installation, database generation, snapshotting, and client CLI install and usage
1. Install `postgres-checkup` tool to automatically check the health of one of your Postgres setups

Both solutions can be integrated with Postgres.ai GUI, so you will have all the meta-data collected in centralized storage. Note that Postgres.ai does not connect to your databases, and only metadata is transferred to Postgres.ai storage. This metadata contains technical details about your database operations. This metadata is encrypted and stored securely and transferred using secure methods (HTTPS). If you have any concerns, please reach out Postgres.ai support using the Intercom widget, which you can find it on the right bottom corner.
